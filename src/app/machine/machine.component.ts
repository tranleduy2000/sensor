import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import Machine from './model/Machine';
import {Observable, Subscriber} from 'rxjs';
import Sensor from './model/Sensor';
import Rect from './model/Rect';

@Component({
  selector: 'app-sensor',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.css']
})
export class MachineComponent implements OnInit, AfterViewInit {

  @Input() demo: boolean;
  @Input() machineProperties: Machine;
  @Input() warningColor = '#ff970d'; // or color hex #AABBCC
  @Input() sensorStrokeWidth = '2';

  @ViewChild('canvasElement') canvasElement: ElementRef;

  constructor() {
  }

  ngOnInit(): void {
    if (this.demo && !this.machineProperties) {
      this.machineProperties = new Machine(
        'Machine 1', '/assets/images/engine.png',
        [
          new Sensor('Sensor 1', new Rect(0.1, 0.12, 0.1, 0.1), true),
          new Sensor('Sensor 2', new Rect(0.3, 0.12, 0.1, 0.1), true),
          new Sensor('Sensor 3', new Rect(0.4, 0.3, 0.1, 0.1), true),
          new Sensor('Sensor 4', new Rect(0.7, 0.5, 0.1, 0.1), true)
        ]
      );
    }
  }

  ngAfterViewInit(): void {
    if (this.machineProperties == null) {
      return;
    }
    this.loadImage(this.machineProperties.imageUrl)
      .subscribe(image => {
        this.drawImageAndSensor(image, this.machineProperties.sensors);
      });
  }


  private loadImage(src: string): Observable<HTMLImageElement> {
    return Observable.create((subscriber: Subscriber<HTMLImageElement>) => {
      const image = new Image();
      image.onload = () => {
        subscriber.next(image);
        subscriber.complete();
      };
      image.onerror = () => {
        subscriber.error();
      };
      image.src = src;
    });
  }

  private convertToImageFrame(imgFrame: Rect, rect: Rect): Rect {
    return new Rect(
      rect.x * imgFrame.w,
      rect.y * imgFrame.h,
      rect.w * imgFrame.w,
      rect.h * imgFrame.h,
    );
  }

  private drawImageAndSensor(image: HTMLImageElement, sensors: Sensor[]): void {
    const canvas: any = this.canvasElement.nativeElement;
    const ctx = canvas.getContext('2d');

    // Scale to fit
    const scale = Math.min(canvas.width / image.width, canvas.height / image.height);
    // Center image
    const x = (canvas.width / 2) - (image.width / 2) * scale;
    const y = (canvas.height / 2) - (image.height / 2) * scale;
    const imgFrame = new Rect(x, y, image.width * scale, image.height * scale);

    ctx.imageSmoothingEnabled = false;
    ctx.drawImage(image, imgFrame.x, imgFrame.y, imgFrame.w, imgFrame.h);


    console.log(sensors);
    sensors.forEach((sensor: Sensor) => {
      if (!sensor.warning) {
        return;
      }
      const sensorFrame = this.convertToImageFrame(imgFrame, sensor.frameInImage);

      // draw sensor frame
      ctx.beginPath();
      ctx.lineWidth = this.sensorStrokeWidth;
      ctx.strokeStyle = this.warningColor;
      ctx.rect(imgFrame.x + sensorFrame.x, imgFrame.y + sensorFrame.y, sensorFrame.w, sensorFrame.h);
      ctx.stroke();

      // draw warning icon
      // set the canvas context's font-size and font-face
      ctx.font = '1rem FontAwesome'; // f071 https://fontawesome.com/v4.7.0/icon/exclamation-triangle
      ctx.fillStyle = this.warningColor;
      const padding = 5;
      ctx.fillText(`\uf071 ${sensor.name}`, imgFrame.x + sensorFrame.x, imgFrame.y + sensorFrame.y - padding);
    });
  }
}
