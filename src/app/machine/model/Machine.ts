import Sensor from './Sensor';

class Machine {
  name: string;
  imageUrl: string;
  sensors: Sensor[];

  constructor(name: string, imageUrl: string, sensors: Sensor[]) {
    this.name = name;
    this.imageUrl = imageUrl;
    this.sensors = sensors;
  }
}
export default Machine;
