export default class Rect {
  x: number;
  y: number;
  w: number;
  h: number;

  /**
   * @param x - 0...1
   * @param y - 0...1
   * @param w - 0...1
   * @param h - 0...1
   */
  constructor(x: number, y: number, w: number, h: number) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
}
