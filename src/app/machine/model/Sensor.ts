import Rect from './Rect';

export default class Sensor {
  name: string;
  warning: boolean;
  frameInImage: Rect;

  constructor(name: string, frameInImage: Rect, warning: boolean) {
    this.name = name;
    this.warning = warning;
    this.frameInImage = frameInImage;
  }
}
